<?php

/**
 * Implements hook_drush_command().
 */
function htools_drush_command() {
  $items = array();

  $items['htools-spam-user-delete'] = array(
    'description' => "Delete all users except for the first user.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['htools-taxonomy-index-rebuild'] = array(
    'description' => 'Rebuild {taxonomy_index} table for faster term-node access.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['htools-referral-show-link'] = array(
    'description' => 'Display the referral link for a user',
    'drupal dependencies' => array('referral'),
    'arguments' => array(
      'username' => 'The user name to show referral link',
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['htools-users-online'] = array(
    'description' => 'Print all online users.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['htools-node-unpublish'] = array(
    'description' => 'Unpublish selected nodes',
    'options' => array(
      'reverse' => 'Whether to reverse unpublish the nodes.'
    ),
    'arguments' => array(
      'node_list' => 'A list of nodes separated by comma',
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['htools-show-user-data'] = array(
    'description' => 'Display the data saved in user->data',
    'arguments' => array(
      'username' => 'The user name to show data',
    ),
    'required-arguments' => TRUE,
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function htools_drush_help($section) {
  switch ($section) {
    case 'drush:spam-user-delete':
      return dt("Delete all users except for the first user.");
  }
}

function drush_htools_spam_user_delete() {
  $uid_list = db_query("SELECT uid FROM {users} WHERE uid > 1")->fetchCol();
  if (drush_confirm('Delete '. count($uid_list) .' users?')) {
    drush_log('Deleting ' . count($uid_list) . ' users.');
    foreach ($uid_list as $uid) {
      // use this instead of user_delete_multiple is to cascade delete spam content.
      user_cancel(array(), $uid, 'user_cancel_delete');
    }
    // see drush_user_cancel().
    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    batch_process();
    drush_log('Deleted ' . count($uid_list) . ' users.', 'ok');
  }
}

function drush_htools_taxonomy_index_rebuild() {
  if (module_exists('taxonomy')) {
    db_delete('taxonomy_index')->execute();
    $nids = db_query('SELECT nid FROM {node} WHERE status = 1')->fetchCol();
    foreach ($nids as $nid) {
      taxonomy_build_node_index(node_load($nid));
    }
    drush_log('Finish rebuilding taxonomy_index.', 'ok');
  }
  else {
    drush_log('Taxonomy module not enabled.', 'warning');
  }
}

function drush_htools_referral_show_link($username) {
  $account = user_load_by_name($username);
  if ($account) {
    $link = url('referral/'. _referral_uid2ref($account->uid), array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE));
    drush_log("Referral link for {$username}: {$link}", 'success');
  }
  else {
    drush_log(t('Cannot find user !username', array('!username' => $username)), 'error');
  }
}

function drush_htools_users_online() {
  $users = htools_online_users();
  if (!empty($users)) {
    drush_print('Online users: '. implode(', ', array_map(function($a) {
        return $a->name;
      }, $users)));
  }
  else {
    drush_print('No online users.');
  }
}

function drush_htools_node_unpublish($node_list) {
  if (empty($node_list)) {
    drush_log('Please specify at least one node id to continue.', 'error');
  }
  $reverse = drush_get_option('reverse', FALSE);

  $list = explode(',', $node_list);
  $nodes = node_load_multiple($list, array(), TRUE);
  $rows = array_map(function ($a) {
    return array($a->nid, $a->title, $a->status);
  }, $nodes);

  drush_print_table($rows);
  if (drush_confirm(t('Are you sure to !action these nodes?', array('!action' => $reverse ? 'publish' : 'unpublish')))) {
    //db_update('node')->fields(array('status' => $reverse ? 1 : 0))->condition('nid', $list)->execute();
    //db_query("UPDATE {node} SET status = :status WHERE nid IN (:list)", array(':status' => $reverse ? 1 : 0, ':list' => $node_list));
    foreach ($nodes as $node) {
      if ($reverse && $node->status == 0) {
        node_publish_action($node);
        node_save($node);
        drush_log('Publish node successful: '. $node->nid, 'ok');
      }
      else if (!$reverse && $node->status == 1) {
        node_unpublish_action($node);
        node_save($node);
        drush_log('Unpublish node successful: '. $node->nid, 'ok');
      }
      else {
        drush_log('Skip: '. $node->nid, 'ok');
      }
    }
    drush_log('Operations done.', 'ok');
  }
}


function drush_htools_show_user_data($username) {
  $account = user_load_by_name($username);
  if (empty($account)) {
    drush_log("Found no user: {$username}", 'error');
  }
  else if (empty($account->data)) {
    drush_log("No data for: {$username}");
  }
  else {
    drush_print_r($account->data);
  }
}